import fs from 'fs';
import path from 'path'

({
	'jsdom': {'file': './quokka.html'},
})

const testDiv = document.getElementById('test');
console.log(testDiv.innerHTML);

	console.log(__dirname == process.cwd())
	console.log(__dirname == path.resolve('./'))

try {
	const text = fs.readFileSync('./src/quokka.txt', 'utf-8');
	console.log( 'text: ', text  );
}
catch(err) {
	console.log( 'Opened Quokka.js as single file || Opened Project with src as root - ', err.message );
}

try {
	const textAbsolute = fs.readFileSync(__dirname + '/quokka.txt', 'utf-8')
	console.log( 'textAbsolute: ', textAbsolute  );
}
catch(err) {
	console.log( 'Opened Quokka.js as single file - ', err.message );
}

try {
	const textRelative = fs.readFileSync('./quokka.txt', 'utf-8')
	console.log( 'textRelative: ', textRelative  );
}
catch(err) {
	console.log( 'Opened Quokka.js as single file || Opened Project - ', err.message );
}




